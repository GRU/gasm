#ifndef _6502_H
#define _6502_H

#define TAX "\xAA"
#define TAY "\xA8"
#define TSX "\xBA"
#define TXA "\x8A"
#define TXS "\x9A"
#define TYA "\x98"

#define INX "\xE8"
#define INY "\xC8"
#define DEX "\xCA"
#define DEY "\x88"

#define NOP "\xEA"

void tax(FILE *outfile);
void tay(FILE *outfile);
void tsx(FILE *outfile);
void txa(FILE *outfile);
void txs(FILE *outfile);
void tya(FILE *outfile);

void inx(FILE *outfile);
void iny(FILE *outfile);
void dex(FILE *outfile);
void dey(FILE *outfile);

void nop(FILE *outfile);

#endif
