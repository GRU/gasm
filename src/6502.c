#include <stdio.h>
#include "6502.h"

void tax(FILE *outfile) {
  fwrite(TAX, 1, 1, outfile);
}

void tay(FILE *outfile) {
  fwrite(TAY, 1, 1, outfile);
}

void tsx(FILE *outfile) {
  fwrite(TSX, 1, 1, outfile);
}

void txa(FILE *outfile) {
  fwrite(TXA, 1, 1, outfile);
}

void txs(FILE *outfile) {
  fwrite(TXS, 1, 1, outfile);
}

void tya(FILE *outfile) {
  fwrite(TYA, 1, 1, outfile);
}

void inx(FILE *outfile) {
  fwrite(INX, 1, 1, outfile);
}

void iny(FILE *outfile) {
  fwrite(INY, 1, 1, outfile);
}

void dex(FILE *outfile) {
  fwrite(DEX, 1, 1, outfile);
}

void dey(FILE *outfile) {
  fwrite(DEY, 1, 1, outfile);
}


void nop(FILE *outfile) {
  fwrite(NOP, 1, 1, outfile);
}

