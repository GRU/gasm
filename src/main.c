#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "6502.h"

char *format;
char lexed_buf[512][128][128];
void lexer(FILE* infile) { // FIXME
	char str[100];
	int line_index = 0;
	while (fgets(str, 100, infile) != NULL){
		int word_index = 0;
		int char_index = 0; // FIXME
		for (size_t i = 0; i <= strlen(str); i++) {
			if (str[i] != ' ' && str[i] != '\n') {
				lexed_buf[line_index][word_index][char_index] = str[i];
				char_index++;
			} else if (str[i] == ' ') {
				word_index++;
			} else if (str[i] == '\n') {
				line_index++;
			}
		}
	}
}

void parser(char lexed_buf[512][128][128], FILE *outfile) { // outfile is needed to add it as argument to command functions
	// TODO
	for (int i = 0; i <= 512; i++) {
		// Parsing lines
		for (int j = 0; j <= 128; j++) {
			if (lexed_buf[i][j] == NULL)
			  break;
			else if (lexed_buf[i][j][0] == '\0')
			  break;
			if (!strcmp(lexed_buf[i][j], "NOP")) {
			  nop(outfile);
			} else if (!strcmp(lexed_buf[i][j], "INX")) {
			  inx(outfile);
			} else if (!strcmp(lexed_buf[i][j], "INY")) {
			  iny(outfile);
			} else if (!strcmp(lexed_buf[i][j], "DEX")) {
			  dex(outfile);
			} else if (!strcmp(lexed_buf[i][j], "DEY")) {
			  dey(outfile);
			} else if (!strcmp(lexed_buf[i][j], "TAX")) {
			  tax(outfile);
			} else if (!strcmp(lexed_buf[i][j], "TAY")) {
			  tay(outfile);
			} else if (!strcmp(lexed_buf[i][j], "TXA")) {
			  txa(outfile);
			} else if (!strcmp(lexed_buf[i][j], "TYA")) {
			  tya(outfile);
			} else if (!strcmp(lexed_buf[i][j], "TSX")) {
			  tsx(outfile);
			} else if (!strcmp(lexed_buf[i][j], "TXS")) {
			  txs(outfile);
			} else {
			  printf("Unrecognized command: %s\n", lexed_buf[i][j]);
			  break;
			}
		}
	}
	return;
}

void usage() {
	printf("%s - GRU Assembler\n", "gasm");
	printf("Usage: %s [OPTIONS] [--] filename \n", "gasm");
	printf("       %s -v\n", "gasm");
	printf("Options:\n");
	printf("\n");
	printf("	--help		display this message\n");
	printf("	-o outfile	write output to file\n");
	// TODO: add more flags
}

int main(int argc, char *argv[]){
	char *infilename = NULL;
	char *outfilename = NULL;
	if (argc == 1) {
		usage();
		return 0;
	} else if (argc >= 2) {
		for (int i = 1; i <= argc; i++) {
			if (argv[i][0] == '-') {
				char *arg = argv[i];
				argv[i]++;
				if(argv[i][0] == '-')
					argv[i]++;
				if (!strcmp(argv[i], "o")) {
					outfilename = argv[i+1];
					i+=2; // To skip next argument
				} else if (!strcmp(argv[i], "help")) {
					usage();
					return 0;
				} else {
					printf("Unknown argument: %s\n", arg);
					return 1;
				}
			} else	{
				infilename = argv[i];
			}
		}
	}
	if (infilename == NULL) {
		printf("No input file\n");
	} else if (outfilename == NULL) {
		printf("No out file\n");
	}
	FILE *infile = fopen(infilename, "r");
	FILE *outfile = fopen(outfilename, "w");

	if (infile == NULL) {
		printf("Failed to open input file\n");
		return 1;
	}
	if (outfile == NULL) {
		printf("Failed to open out file\n");
		return 1;
	}
	
	lexer(infile);

	parser(lexed_buf, outfile);

	fclose(infile);
	fclose(outfile);

	return 0;
}
